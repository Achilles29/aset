<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


$route['dashboard'] = 'home';
$route['login'] = 'auth/login';
$route['account'] = 'home/account';
$route['setting'] = 'home/setting';
$route['logout'] = 'auth/logout';