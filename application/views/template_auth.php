<!DOCTYPE html>
<html>
<head>
    <base href="<?=base_url()?>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=APP_TITLE?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">



    <!-- Font Awesome -->
    <link rel="stylesheet" href="./assets/plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="./assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="./assets/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link href="./assets/plugins/iziToast/css/iziToast.min.css" rel="stylesheet" type="text/css" />
    <?php $this->load->view('js/common')?>
</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="<?=site_url()?>"><b><?=APP_NAME?></b></a>
        </div>
        <!-- /.login-logo -->
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">LOGIN</p>

                <form id="form-login">
                    <div class="input-group mb-3">
                        <input type="text" name="username" class="form-control" placeholder="Username">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" name="password" class="form-control" placeholder="Password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-8">
                            <div class="icheck-primary">
                                <input type="checkbox" name="remember" id="remember">
                                <label for="remember">
                                    Remember Me
                                </label>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-4">
                            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>

            </div>
            <!-- /.login-card-body -->
        </div>
    </div>





    <!-- /.login-box -->

    <!-- jQuery -->
    <script src="./assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="./assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    
    <script src="./assets/plugins/iziToast/js/iziToast.min.js"></script>
    <!-- AdminLTE App -->
    <script src="./assets/js/adminlte.min.js"></script>
    


      


    <script>
        $(function(){
        	$('#form-login').submit(function(e){
				e.preventDefault();
				var form = $(this);
				var formdata = form.serializeArray();
				formdata.push({name: 'csrf_token', value: csrf_token});
				var btn = form.find('button[type="submit"]');
				$.ajax({
					url: '<?=site_url('auth/ajax_login');?>',
					data: formdata,
					dataType: 'json',
					type: 'post',
					beforeSend: function(){
						btn.prop('disabled', true).addClass('btn-warning');
					},
					success: function(d){
						if(d.ok==1){
							mynotif('Berhasil', 'green');
							window.location = d.url;
						}else{
							mynotif(d.msg);
						}
						setTimeout(function(){
							btn.prop('disabled', false).removeClass('btn-warning');
						},2000);
					},
					error: function(){
						mynotif('Tejadi kesalahan');
						setTimeout(function(){
							btn.prop('disabled', false).removeClass('btn-warning');
						},2000);
					}
				});
			});
        })
            
    </script>

</body>
</html>