
<script>
$(function(){
	$('form').submit(function(e){
		e.preventDefault();
		var form = $(this);
		var formdata = form.serializeArray();
		formdata.push({name: 'csrf_token', value: csrf_token});
		var btn = form.find('button[type="submit"]');
		$.ajax({
			url: '<?=site_url('auth/ajax_login');?>',
			data: formdata,
			dataType: 'json',
			type: 'post',
			beforeSend: function(){
				btn.prop('disabled', true).addClass('btn-warning');
			},
			success: function(d){
				if(d.ok==1){
					mynotif('Berhasil', 'green');
					window.location = d.url;
				}else{
					mynotif(d.msg);
				}
				setTimeout(function(){
					btn.prop('disabled', false).removeClass('btn-warning');
				},2000);
			},
			error: function(){
				mynotif('Tejadi kesalahan');
				setTimeout(function(){
					btn.prop('disabled', false).removeClass('btn-warning');
				},2000);
			}
		});
	});
});
</script>
