<div class="row" style="margin-top:40px;">
	<div class="col-md-6 offset-md-3">
		<div class="card">
			<div class="card-header"><i class="fas fa-user"></i> Login</div>
			<div class="card-body">
				<form>
					<div class="form-group">
						<label>Username</label>
						<input type="text" name="username" class="form-control">
					</div>
					<div class="form-group">
						<label>Password</label>
						<input type="password" name="password" class="form-control">
					</div>
					<button type="submit"  class="btn btn-primary" >Submit</button>
				</form>
			</div>
		</div>
		
		
	</div>
</div>