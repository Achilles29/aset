<style type="text/css">
    .mydetail div{
        padding-top:5px;
        padding-bottom:5px;
        border-bottom: 1px solid #c0c0c0;
    }
</style>
<div class="container">
	<form id="form-search" method="get" action="data_kendaraan">
	    <div class="row mb-3">
	        <div class="col-md-4 offset-md-4">
				<div class="konten">
	            <label>Masukan Kata Kunci</label>
				<div class="input-group mb-3">
				  <input type="text" name="s" required class="form-control pencarian" placeholder="Cari Nomor Polisi"  aria-describedby="basic-addon2" value="<?php if ( isset( $_GET['s'] ) ) echo $_GET['s']; ?>">
				  
				    <button class="btn btn-primary btn-sm tombol2" type="submit"><i class="fa fa-search"></i> Cari</button>
				  
				</div>
			</div>
</div>
	    </div>
    </form>

    <div class="row">
		<div class="col-12">
			<?php if( isset( $_GET['s'] ) ): ?>
				<?php if ( empty( $mydata ) ) : ?>
					<p class="text-center">Data tidak ditemukan</p>
				<?php else : ?>
				<div class="card card-primary">
					<div class="card-header">
						<h3 class="card-title">Hasil Pencarian</h3>
					</div>
					<div class="card-body">
						<div class="table-responsive">
						<table id="table-kendaraan" class="table table-bordered dt-responsive wrap" style="width:100%">
						    <thead>
						        <tr>
						            <th>Nomor Polisi</th>
						            <th>Perangkat Daerah</th>
						            <th>Merek</th>
						            <th>Aksi</th>
						        </tr>
						    </thead>
						</table>
				</div>
					</div>
				</div>
				<?php endif; ?>
			<?php else: ?>
			<?php endif; ?>
		</div>
	</div>

    <?php /*
    <div class="row mydata">
        <div class="col-md-8 offset-md-2">
        	<div class="row mydetail">
                <?php  if(empty($mydata)):?>
                <div class="col-md-12">
                    Data tidak ditemukan
                </div>
                <?php else: ?>
                <div class="col-md-3">Nomor Polisi</div>
                <div class="col-md-9"><?=$mydata->Nomor_Polisi?></div>
                <div class="col-md-3">Perangkat Daerah</div>
                <div class="col-md-9"><?=$mydata->Nm_Unit?></div>
                <div class="col-md-3">Kondisi</div>
                <div class="col-md-9"><?=$mydata->Uraian?></div>
                <div class="col-md-3">Merk</div>
                <div class="col-md-9"><?=$mydata->Merk?></div>
                <div class="col-md-3">Type</div>
                <div class="col-md-9"><?=$mydata->Type?></div>
                
                <?php endif?>
            </div>
        </div>
    </div>
    */ ?>

</div>
<div id="copyright">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="site-info">
              <p>© Designed and Developed by <a href="http://setda.rembangkab.go.id" rel="nofollow">Sekretariat Kabupaten Rembang</a></p>
            </div>      
          </div>
        </div>
      </div>
	</div>

	<a href="#" class="back-to-top">
    	<i class="lni-chevron-up"></i>
	</a>
	<br><br>
