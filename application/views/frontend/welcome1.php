<!DOCTYPE html>
<html>

<head>
    <base href="<?=base_url()?>">
    <meta charset="utf-8" />
    <title>
        <?=APP_TITLE?> -
            <?=$title?>
    </title>
    <meta name="description" content="Updates and statistics">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="shortcut icon" href="./assets/img/favicon.png" />
    <link rel="stylesheet" href="./assets/plugins/fontawesome-free/css/all.min.css">
    
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="./assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="./assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="./assets/plugins/jqvmap/jqvmap.min.css">

<!-- Bootstrap CSS -->
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css" >
    <!-- Icon -->
    <link rel="stylesheet" type="text/css" href="assets/fonts/line-icons.css">
    <!-- Slicknav -->
    <link rel="stylesheet" type="text/css" href="assets/css/slicknav.css">
    <!-- Nivo Lightbox -->
    <link rel="stylesheet" type="text/css" href="assets/css/nivo-lightbox.css" >
    <!-- Animate -->
    <link rel="stylesheet" type="text/css" href="assets/css/animate.css">
    <!-- Main Style -->
    <link rel="stylesheet" type="text/css" href="assets/css/main.css">
    <!-- Responsive Style -->
    <link rel="stylesheet" type="text/css" href="assets/css/responsive.css">







    <?php if(isset($css['css']) && !empty($css['css'])): foreach($css['css'] as $link):?>
        <link href="<?=$link?>" rel="stylesheet" type="text/css" />
    <?php endforeach;endif;?>

    <!-- Theme style -->
    <link rel="stylesheet" href="assets/css/adminlte.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="./assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="./assets/plugins/daterangepicker/daterangepicker.css">
    <!-- summernote -->
    <link rel="stylesheet" href="./assets/plugins/summernote/summernote-bs4.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link href="./assets/plugins/iziToast/css/iziToast.min.css" rel="stylesheet" type="text/css" />

    

    <?php if(isset($css['style']) && !empty($css['style'])): foreach($css['style'] as $link):?>
        <?php $this->load->view($link)?>
    <?php endforeach;endif;?>
</head>


<!-- Header Area wrapper Starts -->
<header id="header-wrap">
      <!-- Navbar Start -->
      <nav class="navbar navbar-expand-lg fixed-top scrolling-navbar">
        
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
              <span class="icon-menu"></span>
              <span class="icon-menu"></span>
              <span class="icon-menu"></span>
            </button>
            <div class="logo_kab">
            <a href="#" class="navbar-brand"><img src="assets/img/logo2.png" alt=""><span>Kabupaten Rembang</span></a>
            </div>
          </div>
          <div class="collapse navbar-collapse" id="main-navbar">
            <ul class="navbar-nav mr-auto w-100 justify-content-end">
            
              <li class="nav-item">
                <a class="nav-link" href="#">
                  LOGIN
                </a>
              </li>
            </ul>
          </div>
        

        <!-- Mobile Menu Start -->
        <ul class="mobile-menu">

          <li>
            <a class="page-scroll" href="#google-map-area">LOGIN</a>
          </li>
        </ul>
        <!-- Mobile Menu End -->

      </nav>
      <!-- Navbar End -->

      <!-- Main Carousel Section Start -->
      <div id="main-slide" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <video src="assets/img/Rembang Bangkit.mp4" autoplay loop="true">
          </video>
            <div class="carousel-caption d-md-block">
              <p class="fadeInUp wow" data-wow-delay=".6s">Sistem Informasi Aset</p>
              <h1 class="wow fadeInDown heading" data-wow-delay=".4s">Kabupaten Rembang</h1>
              <a href="<?=site_url('data_kendaraan')?>" class="fadeInRight wow tombol tombol1" data-wow-delay=".6s">Kendaraan</a>
              <a href="#" class="fadeInRight wow tombol tombol1" data-wow-delay=".6s">Tanah</a>
            </div>
          </div>
        </div>
        
      </div>
      <!-- Main Carousel Section End -->

    </header>
    <!-- Header Area wrapper End -->
<!-- Sponsors Section Start -->
<section id="sponsors" class="section-padding">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="section-title-header text-center">
              <h1 class="section-title wow fadeInUp" data-wow-delay="0.2s">Link Penting</h1>
              <p class="wow fadeInDown" data-wow-delay="0.2s">Kabupaten Rembang</p>
            </div>
          </div>
        </div>
        <div class="row mb-30 text-center wow fadeInDown" data-wow-delay="0.3s">
          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="spnsors-logo">
              <a href="http://data.rembangkab.go.id/"><img class="img-fluid" src="assets/img/sponsors/logo-01.png" alt=""><br>Data Rembang</a>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="spnsors-logo">
              <a href="http://sipandu.rembangkab.go.id/"><img class="img-fluid" src="assets/img/sponsors/logo-02.png" alt=""><br>SIPANDU</a>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="spnsors-logo">
              <a href="#"><img class="img-fluid" src="assets/img/sponsors/logo-03.png" alt=""><br>E-Money</a>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="spnsors-logo">
              <a href="#"><img class="img-fluid" src="assets/img/sponsors/logo-04.png" alt=""><br>E-Presensi</a>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="spnsors-logo">
              <a href="#"><img class="img-fluid" src="assets/img/sponsors/logo-04.png" alt=""><br>SIPD</a>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="spnsors-logo">
              <a href="#"><img class="img-fluid" src="assets/img/sponsors/logo-04.png" alt=""><br>E-Sakip</a>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="spnsors-logo">
              <a href="#"><img class="img-fluid" src="assets/img/sponsors/logo-04.png" alt=""><br>LPSE</a>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="spnsors-logo">
              <a href="#"><img class="img-fluid" src="assets/img/sponsors/logo-04.png" alt=""><br>SIKD</a>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="spnsors-logo">
              <a href="#"><img class="img-fluid" src="assets/img/sponsors/logo-04.png" alt=""><br>Simnangkis</a>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="spnsors-logo">
              <a href="#"><img class="img-fluid" src="assets/img/sponsors/logo-04.png" alt=""><br>JDIH</a>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="spnsors-logo">
              <a href="#"><img class="img-fluid" src="assets/img/sponsors/logo-04.png" alt=""><br>E-Retribusi Pasar</a>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="spnsors-logo">
              <a href="#"><img class="img-fluid" src="assets/img/sponsors/logo-04.png" alt=""><br>SIMPATDA</a>
            </div>
          </div>

        </div>
      </div>
    </section>
    
    <!-- Sponsors Section End -->



<body >
    <div class="fluid-container">
        <div class="col-md-4 offset-md-8">
            <a href="<?=site_url('login')?>">Login</a>
        </div>
    </div>
    <?=$contents?>




    <!-- jQuery -->
    <script src="./assets/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="./assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="./assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="./assets/plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="./assets/plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="./assets/plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="./assets/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="./assets/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="./assets/plugins/moment/moment.min.js"></script>
<script src="./assets/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="./assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="./assets/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="./assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="./assets/js/adminlte.js"></script>

    <script src="./assets/plugins/iziToast/js/iziToast.min.js"></script>


        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="assets/js/jquery-min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.countdown.min.js"></script>
    <script src="assets/js/jquery.nav.js"></script>
    <script src="assets/js/jquery.easing.min.js"></script>
    <script src="assets/js/wow.js"></script>
    <script src="assets/js/jquery.slicknav.js"></script>
    <script src="assets/js/nivo-lightbox.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="assets/js/form-validator.min.js"></script>
    <script src="assets/js/contact-form-script.min.js"></script>
    <script src="assets/js/map.js"></script>
    <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyCsa2Mi2HqyEcEnM1urFSIGEpvualYjwwM"></script>


    <?php if(isset($js['js']) && !empty($js['js'])): foreach($js['js'] as $link):?>
        <script src="<?=$link?>" type="text/javascript"></script>
    <?php endforeach;endif;?>

    <?php $this->load->view('js/common');?>

    <?php if(isset($js['script']) && !empty($js['script'])): foreach($js['script'] as $link):?>
    	<?php $this->load->view($link)?>
    <?php endforeach;endif;?>




</body>

</html>