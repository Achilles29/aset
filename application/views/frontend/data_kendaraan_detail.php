<style type="text/css">
    .mydetail div{
        padding-top:5px;
        padding-bottom:5px;
        border-bottom: 1px solid #c0c0c0;
    }
    .mydetail-title{
        font-size: 20px;
    }
</style>
<br><br>
<div class="container">
	<div class="row">
	    <!-- left column -->
	    <div class="col-md-12">
	        <div class="card card-primary">
	            <div class="card-header">
	                <h3 class="card-title">Data Kendaraan</h3>
	            </div>
	            <div class="card-body">
	                <div class="row mydetail">
	                    <?php if(empty($mydata)):?>
	                    <div class="col-md-12">
	                        Data tidak ditemukan
	                    </div>
	                    <?php else: ?>
	                    <div class="col-md-12 text-bold text-center mydetail-title bg-danger">INSTANSI</div>
	                    <div class="col-md-3 text-bold">Provinsi</div>
	                    <div class="col-md-9">Jawa Tengah</div>
	                    <div class="col-md-3 text-bold">Kabupaten / Kota</div>
	                    <div class="col-md-9">Rembang</div>
	                    <div class="col-md-3 text-bold">Unit</div>
	                    <div class="col-md-9"><?=$mydata->Nm_Unit?></div>

	                    <div class="col-md-12 text-bold text-center mydetail-title bg-danger">DETAIL BARANG</div>
	                    <div class="col-md-3 text-bold">Nomor Polisi</div>
	                    <div class="col-md-9"><?=$mydata->Nomor_Polisi?></div>
	                    <div class="col-md-3 text-bold">Merek</div>
	                    <div class="col-md-9"><?=$mydata->Merk?></div>
	                    <div class="col-md-3 text-bold">Type</div>
	                    <div class="col-md-9"><?=$mydata->Type?></div>
	                    <div class="col-md-3 text-bold">CC</div>
	                    <div class="col-md-9"><?=$mydata->CC?></div>
	                    <div class="col-md-3 text-bold">Bahan</div>
	                    <div class="col-md-9"><?=$mydata->Bahan?></div>
	                    <div class="col-md-3 text-bold">Kondisi</div>
	                    <div class="col-md-9"><?=$mydata->Uraian?></div>
	                    <div class="col-md-3 text-bold">Keterangan</div>
	                    <div class="col-md-9"><?=$mydata->Keterangan?></div>

	                    
	                    <?php endif?>
	                </div>

	            </div>
	            <div class="card-footer">
	                <a href="<?=site_url('data_kendaraan')?>" class="btn btn-primary"><i class="fas fa-arrow-circle-left"></i> Kembali</a>
	            </div>
	        </div>
	    </div>
	</div>
</div>
<div id="copyright">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="site-info">
              <p>© Designed and Developed by <a href="http://setda.rembangkab.go.id" rel="nofollow">Sekretariat Kabupaten Rembang</a></p>
            </div>      
          </div>
        </div>
      </div>
	</div>

	<a href="#" class="back-to-top">
    	<i class="lni-chevron-up"></i>
	</a>
	<br><br>