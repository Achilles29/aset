<?php if ( !empty( $mydata ) ) : ?>
<script>
$(function(){
	var data = <?php echo json_encode( $mydata ); ?>;
	var tblTanah = $('#table-kendaraan').DataTable({
		data: data,
		columns: [
            { "data": "nopol" },
            { "data": "perangkat" },
            { "data": "merek" },
            { "data": "action" }
        ]
	});
	// $('#form-search').submit(function(e){
	// 	e.preventDefault();
	// 	tblTanah.ajax.reload();
	// });
});
</script>
<?php endif; ?>
