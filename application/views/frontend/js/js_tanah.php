<?php if ( !empty( $mydata ) ) : ?>
<script>
$(function(){
	var data = <?php echo json_encode( $mydata ); ?>;
	var tblTanah = $('#table-tanah').DataTable({
		data: data,
		columns: [
            { "data": "unit" },
            { "data": "alamat" },
            { "data": "penggunaan" },
            { "data": "action" }
        ]
	});
	// $('#form-search').submit(function(e){
	// 	e.preventDefault();
	// 	tblTanah.ajax.reload();
	// });
});
</script>
<?php endif; ?>
