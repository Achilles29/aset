<br><br>
<style type="text/css">
	.mydetail div{
		padding-top:5px;
		padding-bottom:5px;
		border-bottom: 1px solid #c0c0c0;
	}
</style>

<div class="container">
	<form id="form-search" method="get" action="data_tanah">
		<div class="row mb-3">

			<div class="col-md-4 offset-md-4">
<div class="konten">
				<label>Masukan Kata Kunci</label>
				<div class="input-group mb-3">
					<input type="text" name="s" required class="form-control pencarian" placeholder="Pencarian..."  aria-describedby="basic-addon2" value="<?php if ( isset( $_GET['s'] ) ) echo $_GET['s']; ?>">
					
						<button class="btn btn-primary btn-sm tombol2" type="submit"><i class="fa fa-search"></i> Cari</button>
</div>
				</div>
			</div>

		</div>
	</form>

	<div class="row">
		<div class="col-12">
			<?php if( isset( $_GET['s'] ) ): ?>
				<?php if ( empty( $mydata ) ) : ?>
					<p class="text-center">Data tidak ditemukan</p>
				<?php else : ?>
				<div class="card card-primary">
					<div class="card-header">
						<h3 class="card-title">Hasil Pencarian</h3>
					</div>
					<div class="card-body">
					<div class="table-responsive">
						<table id="table-tanah" class="table table-bordered dt-responsive wrap" style="width:100%">
						    <thead>
						        <tr>
						            <th>Perangkat Daerah</th>
						            <th>Alamat</th>
						            <th>Penggunaan</th>
						            <th>Aksi</th>
						        </tr>
						    </thead>
						</table>
				</div>
					</div>
				</div>
				<?php endif; ?>
			<?php else: ?>
			<?php endif; ?>
		</div>
	</div>

	<?php /*
	<div class="row mydata">
		<div class="col-md-8 offset-md-2">
			<div class="row mydetail">
				<?php  if(empty($mydata)):?>
					<div class="col-md-12">
						Tidak ada data yang dapat ditampilkan.
					</div>
					<?php else: ?>

                <?php /*
                <div class="col-md-3">Perangkat Daerah</div>
                <div class="col-md-9"><?=$mydata->Nm_Unit?></div>
                <div class="col-md-3">Luas</div>
                <div class="col-md-9"><?=$mydata->Luas_M2?></div>
                <div class="col-md-3">Alamat</div>
                <div class="col-md-9"><?=$mydata->Alamat?></div>
                <div class="col-md-3">Hak Tanah</div>
                <div class="col-md-9"><?=$mydata->Hak_Tanah?></div>
                <div class="col-md-3">Penggunaan</div>
                <div class="col-md-9"><?=$mydata->Penggunaan?></div>
                <div class="col-md-3">Keterangan</div>
                <div class="col-md-9"><?=$mydata->Keterangan?></div> 

                <?php foreach( $mydata as $data ) : ?>    
                	<div class="col-12 border-0">
                		<table class="table table-bordered table-sm">
                			<tbody>
                				<tr>
                					<th width="200px">Perangkat Daerah</th>
                					<td><?=$data->Nm_Unit?></td>
                				</tr>
                				<tr>
                					<th>Luas</th>
                					<td><?=$data->Luas_M2?></td>
                				</tr>
                				<tr>
                					<th>Alamat</th>
                					<td><?=$data->Alamat?></td>
                				</tr>
                				<tr>
                					<th>Hak Tanah</th>
                					<td><?=$data->Hak_Tanah?></td>
                				</tr>
                				<tr>
                					<th>Penggunaan</th>
                					<td><?=$data->Penggunaan?></td>
                				</tr>
                				<tr>
                					<th>Keterangan</th>
                					<td><?=$data->Keterangan?></td>
                				</tr>
                			</tbody>
                		</table>
                	</div>
            	<?php endforeach; ?>

        		<?php endif?>
    		</div>
		</div>
	</div>
	*/ ?>
</div>
<div id="copyright">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="site-info">
              <p>© Designed and Developed by <a href="http://setda.rembangkab.go.id" rel="nofollow">Sekretariat Kabupaten Rembang</a></p>
            </div>      
          </div>
        </div>
      </div>
	</div>

	<a href="#" class="back-to-top">
    	<i class="lni-chevron-up"></i>
	</a>
	<br><br>
