<style type="text/css">
    .mydetail div{
        padding-top:5px;
        padding-bottom:5px;
        border-bottom: 1px solid #c0c0c0;
    }
</style>
<div class="container">
	<form method="post">
    <div class="row mb-3">
    	
        <div class="col-md-4 offset-md-4">
            <label>Search</label>
			<div class="input-group mb-3">
			  <input type="text" name="search" required class="form-control" placeholder="Cari Nomor Polisi"  aria-describedby="basic-addon2">
			  <div class="input-group-append">
			    <button class="btn btn-outline-secondary" type="submit"><i class="fa fa-search"></i> Cari</button>
			  </div>
			</div>
        </div>
    	
    </div>
    </form>

    <div class="row mydata">
        <div class="col-md-8 offset-md-2">
        	<div class="row mydetail">
                <?php  if(empty($mydata)):?>
                <div class="col-md-12">
                    Data tidak ditemukan
                </div>
                <?php else: ?>
                <div class="col-md-3">IDPemda</div>
                <div class="col-md-9"><?=$mydata->IDPemda?></div>
                <div class="col-md-3">Nomor Polisi</div>
                <div class="col-md-9"><?=$mydata->Nomor_Polisi?></div>
                <div class="col-md-3">Kondisi</div>
                <div class="col-md-9"><?=$mydata->Uraian?></div>
                
                <?php endif?>
            </div>
        </div>
    </div>
    

</div>
