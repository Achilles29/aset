<div class="row">
	<!-- left column -->
	<div class="col-md-12">
		<div class="card card-primary">
			<div class="card-header">
				<h3 class="card-title">Dashboard</h3>
			</div>
			<div class="card-body">

				<section class="content">
					<div class="container-fluid">
						<!-- Small boxes (Stat box) -->
						<div class="row">
							<div class="col-lg-6 col-10">
								<!-- small box -->
								<div class="small-box bg-info">
									<div class="inner">
										<h4><b> Jumlah Aset : 
											<?php 

											echo $jumlah;
											?>

										</b></h4>

										<p><b>Kendaraan</b></p>
									</div>
									<div class="icon">
										<i class="fa fa-car"></i>
									</div>
									<a href="<?=site_url('kendaraan')?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
								</div>
							</div>
							<!-- ./col -->

							<div class="col-lg-6 col-10">
								<!-- small box -->
								<div class="small-box bg-danger">
									<div class="inner">
										<h4><b> Jumlah Aset : 
											<?php 

											echo $jumlah_tanah;
											?>


										</b></h4>

										<p><b>Tanah</b></p>

									</div>
									<div class="icon">
										<i class="fa fa-globe"></i>
									</div>
									<a href="<?=site_url('tanah')?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
								</div>
							</div>
							<!-- ./col -->
						</div>
					</div><!-- /.container-fluid -->
				</section>

			</div>
		</div>
	</div>
</div>

<div class="row">
    <div class="col-12 col-lg-6">
        <div class="card card-primary">
        	<div class="card-header">
        		<h3 class="card-title"><i class="fa fa-car"></i> Kondisi Kendaraan</h3>
        	</div>
            <div class="card-body">
                <canvas id="myChart"></canvas>
            </div>
        </div>
    </div>
</div>