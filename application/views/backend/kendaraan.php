<style type="text/css">
    .box-action{
        width:40px;
    }
</style>

<div class="row">

<?php

?>
    <!-- left column -->
    <div class="col-md-12">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Data Kendaraan</h3>
            </div>
            <div class="card-body">
                <form id="form-search" role="form">
                    <div class="row mb-3">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Nomor Polisi</label>
                                <input type="text" name="Nomor_Polisi" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Perangkat Daerah</label>
                                <input type="text" name="Nm_Unit" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Merek</label>
                                <input type="text" name="Merk" class="form-control">
                            </div>
                        </div>                      
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Kondisi</label>
                                <select name="Kondisi" class="form-control">
                                    <option value="">Semua</option>
                                    <?php foreach($Kondisi as $row):?>
                                    <option value="<?=$row->Kd_Kondisi?>"><?=$row->Uraian?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>
                        
                        
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-4  ">
                            <button type="submit" class=" btn btn-primary"><i class="fa fa-search"></i> Submit</button>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-md-12">
                    <div class="table-responsive">
                        <table id="table-kendaraan" class="table responsive" style="width:100%">
                            <thead>
                                <tr>
                                    <th>IdPemda</th>
                                    <th>No Polisi</th>
                                    <th>Unit</th>
                                    <th>Merk</th>
                                    <th>Type</th>
                                    <th>Kondisi</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>    
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


