
 <!-- Sidebar Menu -->
  <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item">
              <a href="<?=site_url('dashboard')?>" class="nav-link">
                  <i class="nav-icon fas fa-home"></i>
                  <p>Dashboard</p>
              </a>
        </li>
        <li class="nav-item">
              <a href="<?=site_url('kendaraan')?>" class="nav-link">
                  <i class="nav-icon fas fa-truck"></i>
                  <p>Kendaraan</p>
              </a>
        </li>
        <li class="nav-item">
              <a href="<?=site_url('tanah')?>" class="nav-link">
                  <i class="nav-icon fas fa-cog"></i>
                  <p>Tanah</p>
              </a>
        </li>
        <li class="nav-item">
              <a href="<?=site_url('logout')?>" class="nav-link">
                  <i class="nav-icon fas fa-sign-out-alt"></i>
                  <p>Logout</p>
              </a>
        </li>
          
      </ul>
  </nav>
  <!-- /.sidebar-menu -->