<style type="text/css">
    .mydetail div{
        padding-top:5px;
        padding-bottom:5px;
        border-bottom: 1px solid #c0c0c0;
    }
    .mydetail-title{
        font-size: 20px;
    }
</style>

<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Data Kendaraan</h3>
            </div>
            <div class="card-body">
                <div class="row mydetail">
                    <?php if(empty($mydata)):?>
                    <div class="col-md-12">
                        Data tidak ditemukan
                    </div>
                    <?php else: ?>
                    <div class="col-md-12 text-bold text-center mydetail-title bg-danger">INSTANSI</div>
                    <div class="col-md-3 text-bold">IDPemda</div>
                    <div class="col-md-9"><?=$mydata->IDPemda?></div>
                    <div class="col-md-3 text-bold">Provinsi</div>
                    <div class="col-md-9">Jawa Tengah</div>
                    <div class="col-md-3 text-bold">Kabupaten / Kota</div>
                    <div class="col-md-9">Rembang</div>
                    <div class="col-md-3 text-bold">Unit</div>
                    <div class="col-md-9"><?=$mydata->Nm_Unit?></div>
                    <div class="col-md-3 text-bold">Sub Unit</div>
                    <div class="col-md-9"><?=$mydata->Nm_Sub_Unit?></div>
                    <div class="col-md-3 text-bold">UPB</div>
                    <div class="col-md-9"><?=$mydata->Nm_UPB?></div>

                    <div class="col-md-12 text-bold text-center mydetail-title bg-danger">KUASA BARANG</div>
                    <div class="col-md-3 text-bold">Ruang</div>
                    <div class="col-md-9"><?=$mydata->Nm_Ruang?></div>
                    <div class="col-md-3 text-bold">Jabatan PJ</div>
                    <div class="col-md-9"><?=$mydata->Jbt_Pngjwb?></div>
                    <div class="col-md-3 text-bold">Nama PJ</div>
                    <div class="col-md-9"><?=$mydata->Nm_Pngjwb?></div>
                    <div class="col-md-3 text-bold">NIP PJ</div>
                    <div class="col-md-9"><?=$mydata->Nip_Pngjwb?></div>

                    <div class="col-md-12 text-bold text-center mydetail-title bg-danger">DETAIL BARANG</div>
                    <div class="col-md-3 text-bold">Nomor Polisi</div>
                    <div class="col-md-9"><?=$mydata->Nomor_Polisi?></div>
                    <div class="col-md-3 text-bold">Merek</div>
                    <div class="col-md-9"><?=$mydata->Merk?></div>
                    <div class="col-md-3 text-bold">Type</div>
                    <div class="col-md-9"><?=$mydata->Type?></div>
                    <div class="col-md-3 text-bold">CC</div>
                    <div class="col-md-9"><?=$mydata->CC?></div>
                    <div class="col-md-3 text-bold">Bahan</div>
                    <div class="col-md-9"><?=$mydata->Bahan?></div>
                    <div class="col-md-3 text-bold">Nomor Rangka</div>
                    <div class="col-md-9"><?=$mydata->Nomor_Rangka?></div>
                    <div class="col-md-3 text-bold">Nomor Mesin</div>
                    <div class="col-md-9"><?=$mydata->Nomor_Mesin?></div>
                    <div class="col-md-3 text-bold">Nomor BPKB</div>
                    <div class="col-md-9"><?=$mydata->Nomor_BPKB?></div>
                    <div class="col-md-3 text-bold">Tanggal Perolehan</div>
                    <div class="col-md-9"><?=tgl_indo($mydata->Tgl_Perolehan)?></div>
                    <div class="col-md-3 text-bold">Kondisi</div>
                    <div class="col-md-9"><?=$mydata->Uraian?></div>
                    <div class="col-md-3 text-bold">Tanggal Perolehan</div>                   
					<div class="col-md-9"><?php echo substr ($mydata->Tgl_Perolehan, 0, -13); ?></div>
                    <div class="col-md-3 text-bold">Asal_usul</div>
                    <div class="col-md-9"><?=$mydata->Asal_usul?></div>
                    <div class="col-md-3 text-bold">Harga</div>
					<div class="col-md-9"><?php echo "Rp. ".number_format($mydata->Harga, 2); ?></div>
                    <div class="col-md-3 text-bold">Tanggal Pembukuan</div>                   
					<div class="col-md-9"><?php echo substr ($mydata->Tgl_Pembukuan, 0, -13); ?></div>
                    <div class="col-md-3 text-bold">Keterangan</div>
                    <div class="col-md-9"><?=$mydata->Keterangan?></div>

                    
                    <?php endif?>
                </div>

            </div>
            <div class="card-footer">
                <a href="<?=site_url('kendaraan')?>" class="btn btn-primary"><i class="fas fa-arrow-circle-left"></i> Kembali</a>
            </div>
        </div>
    </div>
</div>

