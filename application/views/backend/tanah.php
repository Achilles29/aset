<style type="text/css">
    .box-action{
        width:40px;
    }
</style>

<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Data Tanah</h3>
            </div>
            <div class="card-body">
                <form id="form-search" role="form">
                    <div class="row mb-3">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>ID Pemda</label>
                                <input type="text" name="IDPemda" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Unit</label>
                                <input type="text" name="Nm_Unit" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Alamat</label>
                                <input type="text" name="Alamat" class="form-control">
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Nomor Sertifikat</label>
                                <input type="text" name="Sertifikat_Nomor" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Hak Tanah</label>
                                <select name="Hak_Tanah" class="form-control">
                                    <option value="semua">Semua</option>
                                    <?php if($Hak_Tanah):foreach($Hak_Tanah as $row):?>
                                    <option value="<?=$row->Hak_Tanah?>"><?=$row->Hak_Tanah?></option>
                                    <?php endforeach;endif;?>
                                </select>
                            </div>
                        </div>            
						<div class="col-md-12 ">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Submit</button>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-md-12">
                        <table id="table-tanah" class="table table-bordered dt-responsive wrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th>IdPemda</th>
                                    <th>Unit</th>
                                    <th>Alamat</th>
                                    <th>No Sertifikat</th>
                                    <th>Hak Tanah</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

