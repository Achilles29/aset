<script type="text/javascript">
	var ctx = document.getElementById('myChart').getContext('2d');
	var color = {
		red: 'rgb(255, 99, 132)',
		orange: 'rgb(255, 159, 64)',
		yellow: 'rgb(255, 205, 86)',
		green: 'rgb(75, 192, 192)',
		blue: 'rgb(54, 162, 235)',
		purple: 'rgb(153, 102, 255)',
		grey: 'rgb(201, 203, 207)'
	};

	var kondisi = <?php echo json_encode( $kondisi ); ?>;
	var label_kondisi = kondisi.map(a => a.Uraian);

	var data = {
	    datasets: [{ 
	    	data: [10, 20, 30],
	    	backgroundColor: [
	    		color.green,
	    		color.yellow,
	    		color.red,
	    		color.grey,
	    		color.orange,
	    		color.blue
	    	],
	    }],
	    labels: label_kondisi
	};

	var chart = new Chart(ctx, {
	   	type: 'pie',
	    data: data,
	    options: {}
	});
</script>