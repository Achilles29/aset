
<script>
$(function(){
	
	
	var tblKendaraan = $('#table-kendaraan').DataTable({
		processing: true,
        serverSide: true,
        ajax: {
			url: '<?=site_url('kendaraan/datatables_kendaraan')?>',
			type: 'post',
			data: function(d){
				d.Nomor_Polisi = $('[name="Nomor_Polisi"]').val();
				d.Nm_Unit = $('[name="Nm_Unit"]').val();
				d.Merk = $('[name="Merk"]').val();
				d.Kondisi = $('[name="Kondisi"]').val();
			}
		},
		columnDefs: [
			{ responsivePriority: 1, targets: 0 },
			{ responsivePriority: 2, targets: -1 }
		],
		order: [[ 0, "desc" ]]
	});
	
	$('#form-search').submit(function(e){
		e.preventDefault();
		tblKendaraan.ajax.reload();
	});
	
	
});
</script>
