
<script>
$(function(){
	
	
	var tblTanah = $('#table-tanah').DataTable({
		processing: true,
        serverSide: true,
        ajax: {
			url: '<?=site_url('tanah/datatables_tanah')?>',
			type: 'post',
			data: function(d){
				console.log(d);
				d.IDPemda = $('[name="IDPemda"]').val();
				d.Nm_Unit = $('[name="Nm_Unit"]').val();
				d.Alamat = $('[name="Alamat"]').val();
				d.Sertifikat_Nomor = $('[name="Sertifikat_Nomor"]').val();
				d.Hak_Tanah = $('[name="Hak_Tanah"]').val();
			}
		},
		columnDefs: [
			{ responsivePriority: 1, targets: 0 },
			{ responsivePriority: 2, targets: -1 }
		],
		order: [[ 0, "desc" ]]
	});
	
	$('#form-search').submit(function(e){
		e.preventDefault();
		tblTanah.ajax.reload();
	});
	
	
});
</script>
