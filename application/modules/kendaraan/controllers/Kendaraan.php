<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kendaraan extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!isLoggedIn() && !is_cli()) redirect('login');
	}
	
	public function index()
	{
		$css['css']= ['./assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css'];
		$js['js']= ['./assets/plugins/datatables/jquery.dataTables.js','./assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js'];
		$js['script']= ['backend/js/kendaraan'];
		$data['Kondisi'] = $this->db->get('Ref_Kondisi')->result();
		backendTemplate('backend/kendaraan', 'Kendaraan', $data, $css, $js);
	}

	public function detail($IDPemda=null){
		$css = [];
		$js = [];
		
		$this->db->select('a.*,b.Nm_Unit, e.Nm_Sub_Unit, f.Nm_UPB, c.Nm_Ruang, c.Jbt_Pngjwb, c.Nip_Pngjwb, c.Nm_Pngjwb, d.Uraian')->from('Ta_KIB_B a')
			->join('Ref_Unit b','(a.Kd_Bidang = b.Kd_Bidang And a.Kd_Unit = b.Kd_Unit)')
			->join('Ta_Ruang c','(a.Kd_Bidang = c.Kd_Bidang and 
				a.Kd_Unit = c.Kd_Unit and 
				a.Kd_Sub = c.Kd_Sub and 
				a.Kd_UPB = c.Kd_UPB and 
				a.Kd_Ruang = c.Kd_Ruang)','left')
			->join('Ref_Sub_Unit e','(a.Kd_Bidang = e.Kd_Bidang and 
				a.Kd_Unit = e.Kd_Unit and 
				a.Kd_Sub = e.Kd_Sub)','left')
			->join('Ref_UPB f','(a.Kd_Bidang = f.Kd_Bidang and 
				a.Kd_Unit = f.Kd_Unit and 
				a.Kd_Sub = f.Kd_Sub and 
				a.Kd_UPB = f.Kd_UPB)','left')
			->join('Ref_Kondisi d','a.Kondisi=d.Kd_Kondisi')
			->where('a.IDPemda',$IDPemda);
		if(!isAdmin()){
			$userBidang = session('bidang');
			$userUnit = session('unit');
			$userSubUnit = session('sub');
			$userUpb = session('upb');
			if(!empty($userBidang)) $this->db->where('a.Kd_Bidang',$userBidang);
			if(!empty($userUnit)) $this->db->where('a.Kd_Unit',$userUnit);
			if(!empty($userSubUnit)) $this->db->where('a.Kd_Sub',$userSubUnit);
			if(!empty($userUpb)) $this->db->where('a.Kd_UPB',$userUpb);
		}
		$data['mydata'] =  $this->db->get()->row();
		backendTemplate('backend/kendaraan_detail', 'Detail Kendaraan', $data, $css, $js);
	}
	
	
	public function datatables_kendaraan(){
		
		$draw = (int) post('draw');
		$start = (int) post('start');
		$length = (int) post('length');
		$Merk = post('Merk');
		$Type = post('Type');
		$Nm_Unit = post('Nm_Unit');
		$Kondisi = post('Kondisi');
		$Nomor_Polisi = post('Nomor_Polisi');
		$isAdmin = isAdmin();
		$userBidang = session('bidang');
		$userUnit = session('unit');
		$userSubUnit = session('sub');
		$userUpb = session('upb');

		$selectColumn = ['a.IDPemda', 'a.Nomor_Polisi', 'b.Nm_Unit', 'a.Merk', 'a.Type', 'd.Uraian'];
		$searchColumn = ['a.IDPemda', 'a.Nomor_Polisi', 'b.Nm_Unit', 'a.Merk', 'a.Type', 'd.Uraian'];
		$orderColumn = ['a.IDPemda', 'a.Nomor_Polisi', 'b.Nm_Unit', 'a.Merk', 'a.Type', 'd.Uraian'];


		$this->db->from('Ta_KIB_B a')
		->join('Ref_Unit b','a.Kd_Bidang = b.Kd_Bidang And a.Kd_Unit = b.Kd_Unit')
		->join('Ref_Kondisi d','a.Kondisi=d.Kd_Kondisi')
		;


		if(!$isAdmin){
			if(!empty($userBidang)) $this->db->where('a.Kd_Bidang',$userBidang);
			if(!empty($userUnit)) $this->db->where('a.Kd_Unit',$userUnit);
			if(!empty($userSubUnit)) $this->db->where('a.Kd_Sub',$userSubUnit);
			if(!empty($userUpb)) $this->db->where('a.Kd_UPB',$userUpb);
		}


		$totalAll = $this->db->count_all_results('', FALSE);



		if(!empty($Merk)){
			$this->db->where('a.Merk',$Merk); //where harus sama persis, Toyotaku <> Toyota
		}
		
		if(!empty($Nm_Unit)){
			$this->db->like('b.Nm_Unit',$Nm_Unit); 
		}
		if(!empty($Kondisi)){
			$this->db->where('a.Kondisi',$Kondisi); 
		}
		if(!empty($Nomor_Polisi)){
			$this->db->like('a.Nomor_Polisi',$Nomor_Polisi); //like g mesti sama , hanya beberapa part dari huruf aja. inDONEsia = done 
		}
		
		$search = post('search');
		if(isset($search['value']) && !empty($search['value'])){
			$this->db->group_start();
			foreach($searchColumn as $col){
				$this->db->or_like($col, $search['value']);
			}
			$this->db->group_end();
		}
		
		$totalFilter = $this->db->count_all_results('', FALSE);
		
		$order = post('order');
		if(isset($order) && !empty($order)){
			foreach($order as $i=>$col){
				$this->db->order_by($orderColumn[$order[$i]['column']], $order[$i]['dir']);
			}
		}
		
		$this->db->select($selectColumn);
		$this->db->limit($length, $start);
		$q = $this->db->get();
		$data=[];
		$statusLabel = ['Disabled', 'Enabled'];
		if($q->num_rows() >0){
			foreach($q->result() as $row){
				$d = [];
				$d[] = $row->IDPemda;
				$d[] = $row->Nomor_Polisi;
				$d[] = $row->Nm_Unit;
				$d[] = $row->Merk;
				$d[] = $row->Type;
				$d[] = $row->Uraian;
				$d[] = '<div class="box-action">
							<a href="'.site_url('kendaraan/detail/'.$row->IDPemda).'" class="btn btn-sm btn-info" title="View"><i class="fas fa-eye"></i></button>
                        </div>';
				$data[] = $d;
			}
		}
		
		$res['draw'] = $draw;
		$res['recordsTotal'] = $totalAll;
		$res['recordsFiltered'] = $totalFilter;
		$res['data'] = $data;
		ms($res);
	}	
	
}
