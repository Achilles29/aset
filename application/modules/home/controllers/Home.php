<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MX_Controller {
	
	public function __construct(){
		parent::__construct();
		if(!isLoggedIn() && !is_cli()) redirect('login');
	}

	public function index()
	{
		$css['css']= [];
		$js['js']= ['https://www.chartjs.org/samples/latest/utils.js','https://cdn.jsdelivr.net/npm/chart.js@2.8.0'];
		$js['script']= ['backend/js/js_dashboard.php'];
		$data['jumlah']=$this->jumlah();
		$data['jumlah_tanah']=$this->jumlah_tanah();
		$data['kondisi'] = $this->db->get('Ref_Kondisi')->result();

		backendTemplate('backend/dashboard', 'Dashboard', $data, $css, $js);
	}


	public function jumlah()
	{

		$userBidang = session('bidang');
		$userUnit = session('unit');
		$userSubUnit = session('sub');
		$userUpb = session('upb');
		$this->db->select('count(*) as total');

		$this->db->from('Ta_KIB_B a')
		->join('Ref_Unit b','a.Kd_Bidang = b.Kd_Bidang And a.Kd_Unit = b.Kd_Unit');

		if(!isAdmin()){
			if(!empty($userBidang)) $this->db->where('a.Kd_Bidang',$userBidang);
			if(!empty($userUnit)) $this->db->where('a.Kd_Unit',$userUnit);
			if(!empty($userSubUnit)) $this->db->where('a.Kd_Sub',$userSubUnit);
			if(!empty($userUpb)) $this->db->where('a.Kd_UPB',$userUpb);
		}	
		$jumlah = (int) $this->db->get()->row()->total;
		return $jumlah;
	}

	public function jumlah_tanah()
	{
		$userBidang = session('bidang');
		$userUnit = session('unit');
		$userSubUnit = session('sub');
		$userUpb = session('upb');
		$this->db->select('count(*) as total');

		$this->db->from('Ta_KIB_A a')
		->join('Ref_Unit b','a.Kd_Bidang = b.Kd_Bidang And a.Kd_Unit = b.Kd_Unit');

		if(!isAdmin()){
			if(!empty($userBidang)) $this->db->where('a.Kd_Bidang',$userBidang);
			if(!empty($userUnit)) $this->db->where('a.Kd_Unit',$userUnit);
			if(!empty($userSubUnit)) $this->db->where('a.Kd_Sub',$userSubUnit);
			if(!empty($userUpb)) $this->db->where('a.Kd_UPB',$userUpb);
		}	
		$jumlah_tanah = (int) $this->db->get()->row()->total;
		return $jumlah_tanah;
	}

	public function page404()
	{
		$css= [];
		$js= [];
		$data= [];
		$this->load->view('errors/html/error_404',  $data);
	}
	
}
