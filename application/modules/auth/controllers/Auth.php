<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MX_Controller {

	
	public function login()
	{
		if(isLoggedIn()) redirect('dashboard');
		$css= [];
		$js['script']= ['auth/js/login'];
		$data= [];
		authTemplate('auth/login', 'Login', $data, $css, $js);
	}
	
	public function ajax_login(){
		if(isLoggedIn()) redirect('dashboard');
		$username = trim(post('username'));
		$password = post('password');
		
		$res['ok']=0;
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		
		if(!$this->form_validation->run()){
			$errors = $this->form_validation->error_array();
			$res['msg'] = array_values($errors)[0];
		}else{
			$q = $this->db->select('a.*, b.Kd_Bidang,b.Kd_Unit,b.Kd_Sub, b.Kd_UPB')->from('Ta_UserID a')->join('Ta_User_Satker b','a.User_ID=b.User_ID','left')->where(['a.User_ID'=>$username, 'a.Pwd'=>$password])->get()->row();
			if($q){
				$data =[
					'loggedIn'=>true,
					'user_id'=>$q->User_ID,
					'fullname'=>$q->Full_Name,
					'keterangan'=>$q->Keterangan,
					'kd_level'=>$q->Kd_Level,
					'v_group_id'=>$q->V_Group_ID,
					'bidang'=>$q->Kd_Bidang,
					'unit'=>$q->Kd_Unit,
					'sub'=>$q->Kd_Sub,
					'upb'=>$q->Kd_UPB,
				];
				$this->session->set_userdata($data);
				$res['ok']=1;
				$res['url']=site_url('dashboard');
			}else{
				$res['msg'] = 'wrong username/password';	
			}
			
		}
		ms($res);
	}
	
	function logout(){
		$this->session->sess_destroy();
		$_SESSION=NULL;
		redirect();
	}
}
