<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tanah extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!isLoggedIn() && !is_cli()) redirect('login');
	}
	
	public function index()
	{
		$css['css']= ['./assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css'];
		$js['js']= ['./assets/plugins/datatables/jquery.dataTables.js','./assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js'];
		$js['script']= ['backend/js/tanah'];
		$data['Hak_Tanah'] = $this->db->select('DISTINCT(Hak_Tanah) AS Hak_Tanah')->get('Ta_KIB_A')->result();
		backendTemplate('backend/tanah', 'Tanah', $data, $css, $js);
	}

	public function detail($IDPemda=null){
		$css = [];
		$js = [];
		
		$this->db->select('a.*,b.Nm_Unit, e.Nm_Sub_Unit, f.Nm_UPB')->from('Ta_KIB_A a')
			->join('Ref_Unit b','(a.Kd_Bidang = b.Kd_Bidang And a.Kd_Unit = b.Kd_Unit)')
			->join('Ref_Sub_Unit e','(a.Kd_Bidang = e.Kd_Bidang and 
				a.Kd_Unit = e.Kd_Unit and 
				a.Kd_Sub = e.Kd_Sub)','left')
			->join('Ref_UPB f','(a.Kd_Bidang = f.Kd_Bidang and 
				a.Kd_Unit = f.Kd_Unit and 
				a.Kd_Sub = f.Kd_Sub and 
				a.Kd_UPB = f.Kd_UPB)','left')
			->where('a.IDPemda',$IDPemda);

			
			
			
		if(!isAdmin()){
			$userBidang = session('bidang');
			$userUnit = session('unit');
			$userSubUnit = session('sub');
			$userUpb = session('upb');
			if(!empty($userBidang)) $this->db->where('a.Kd_Bidang',$userBidang);
			if(!empty($userUnit)) $this->db->where('a.Kd_Unit',$userUnit);
			if(!empty($userSubUnit)) $this->db->where('a.Kd_Sub',$userSubUnit);
			if(!empty($userUpb)) $this->db->where('a.Kd_UPB',$userUpb);
		}
		$data['mydata'] =  $this->db->get()->row();
		
		backendTemplate('backend/tanah_detail', 'Detail Tanah', $data, $css, $js);
	}
	
	
	public function datatables_tanah(){
		
		$draw = (int) post('draw');
		$start = (int) post('start');
		$length = (int) post('length');
		$IDPemda = post('IDPemda');
		$Nm_Unit = post('Nm_Unit');
		$Alamat = post('Alamat');
		$Sertifikat_Nomor = post('Sertifikat_Nomor');
		$Hak_Tanah = post('Hak_Tanah');
		$isAdmin = isAdmin();
		$userBidang = session('bidang');
		$userUnit = session('unit');
		$userSubUnit = session('sub');
		$userUpb = session('upb');

		$selectColumn = ['a.IDPemda', 'b.Nm_Unit', 'a.Alamat', 'a.Sertifikat_Nomor', 'a.Hak_Tanah'];
		$searchColumn = ['a.IDPemda', 'b.Nm_Unit', 'a.Alamat', 'a.Sertifikat_Nomor', 'a.Hak_Tanah'];
		$orderColumn = ['a.IDPemda', 'b.Nm_Unit', 'a.Alamat', 'a.Sertifikat_Nomor', 'a.Hak_Tanah'];


		$this->db->from('Ta_KIB_A a')
		->join('Ref_Unit b','a.Kd_Bidang = b.Kd_Bidang And a.Kd_Unit = b.Kd_Unit')
		;


		if(!$isAdmin){
			if(!empty($userBidang)) $this->db->where('a.Kd_Bidang',$userBidang);
			if(!empty($userUnit)) $this->db->where('a.Kd_Unit',$userUnit);
			if(!empty($userSubUnit)) $this->db->where('a.Kd_Sub',$userSubUnit);
			if(!empty($userUpb)) $this->db->where('a.Kd_UPB',$userUpb);
		}


		$totalAll = $this->db->count_all_results('', FALSE);


		if(!empty($IDPemda)){
			$this->db->like('a.IDPemda',$IDPemda); 
		}

		if(!empty($Nm_Unit)){
			$this->db->like('b.Nm_Unit',$Nm_Unit); 
		}

		if(!empty($Alamat)){
			$this->db->like('a.Alamat',$Alamat); 
		}
		
		if(!empty($Sertifikat_Nomor)){
			$this->db->like('a.Sertifikat_Nomor',$Sertifikat_Nomor); 
		}
		if($Hak_Tanah !='semua'){
			$this->db->where('a.Hak_Tanah',$Hak_Tanah); 
		}
		
		$search = post('search');
		if(isset($search['value']) && !empty($search['value'])){
			$this->db->group_start();
			foreach($searchColumn as $col){
				$this->db->or_like($col, $search['value']);
			}
			$this->db->group_end();
		}
		
		$totalFilter = $this->db->count_all_results('', FALSE);
		
		$order = post('order');
		if(isset($order) && !empty($order)){
			foreach($order as $i=>$col){
				$this->db->order_by($orderColumn[$order[$i]['column']], $order[$i]['dir']);
			}
		}
		
		$this->db->select($selectColumn);
		$this->db->limit($length, $start);
		$q = $this->db->get();
		$data=[];
		$statusLabel = ['Disabled', 'Enabled'];
		if($q->num_rows() >0){
			foreach($q->result() as $row){
				$d = [];
				$d[] = $row->IDPemda;
				$d[] = $row->Nm_Unit;
				$d[] = $row->Alamat;
				$d[] = $row->Sertifikat_Nomor;
				$d[] = $row->Hak_Tanah;
				$d[] = '<div class="box-action">
							<a href="'.site_url('tanah/detail/'.$row->IDPemda).'" class="btn btn-sm btn-info" title="View"><i class="fas fa-eye"></i></button>
                        </div>';
				$data[] = $d;
			}
		}
		
		$res['draw'] = $draw;
		$res['recordsTotal'] = $totalAll;
		$res['recordsFiltered'] = $totalFilter;
		$res['data'] = $data;
		ms($res);
	}
	
}
