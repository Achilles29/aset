<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_tanah extends MX_Controller {
	
	public function __construct(){
		parent::__construct();
		
	}

	public function index()
	{
		// $IDPemda=post('data_tanah');
		// $css['css']= [];
		// $js['js']= [];
		// $js['script']= [];
		
		$css['css']= ['./assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css'];
		$js['js']= ['./assets/plugins/datatables/jquery.dataTables.js','./assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js'];
		$js['script']= ['frontend/js/js_tanah'];

		$IDPemda=get('s');
		$data = array();
		if(!empty($IDPemda)){
			// $this->db->select('a.*,b.Nm_Unit, e.Nm_Sub_Unit, f.Nm_UPB')->from('Ta_KIB_A a')
			// ->join('Ref_Unit b','(a.Kd_Bidang = b.Kd_Bidang And a.Kd_Unit = b.Kd_Unit)')
			// ->join('Ref_Sub_Unit e','(a.Kd_Bidang = e.Kd_Bidang and 
			// 	a.Kd_Unit = e.Kd_Unit and 
			// 	a.Kd_Sub = e.Kd_Sub)','left')
			// ->join('Ref_UPB f','(a.Kd_Bidang = f.Kd_Bidang and 
			// 	a.Kd_Unit = f.Kd_Unit and 
			// 	a.Kd_Sub = f.Kd_Sub and 
			// 	a.Kd_UPB = f.Kd_UPB)','left')
   //          // ->where('a.IDPemda',$IDPemda);
   //          ->like('a.IDPemda', $IDPemda)
   //          ->or_like('a.Kd_Unit', $IDPemda)
			// ->or_like('b.Nm_Unit', $IDPemda)
			// ->or_like('a.Alamat', $IDPemda)
			// ->or_like('a.Sertifikat_Nomor', $IDPemda);
            // $mydata = $this->db->get()->row();
            
            $selectColumn = ['a.*', 'a.IDPemda', 'b.Nm_Unit', 'a.Alamat', 'a.Sertifikat_Nomor', 'a.Hak_Tanah'];
			$searchColumn = ['a.IDPemda', 'b.Nm_Unit', 'a.Alamat', 'a.Sertifikat_Nomor', 'a.Hak_Tanah'];

			$this->db->select($selectColumn);

			$this->db->from('Ta_KIB_A a')
			->join('Ref_Unit b','(a.Kd_Bidang = b.Kd_Bidang And a.Kd_Unit = b.Kd_Unit)')
			->join('Ref_Sub_Unit e','(a.Kd_Bidang = e.Kd_Bidang and 
				a.Kd_Unit = e.Kd_Unit and 
				a.Kd_Sub = e.Kd_Sub)','left')
			->join('Ref_UPB f','(a.Kd_Bidang = f.Kd_Bidang and 
				a.Kd_Unit = f.Kd_Unit and 
				a.Kd_Sub = f.Kd_Sub and 
				a.Kd_UPB = f.Kd_UPB)','left');

			$this->db->group_start();
			foreach($searchColumn as $col){
				$this->db->or_like($col, $IDPemda);
			}
			$this->db->group_end();

            $q = $this->db->get();
            if ( $q->num_rows() > 0 ) {
				foreach($q->result() as $row){
					$action = '<div class="box-action">
							<a href="'.site_url('data_tanah/detail/'.$row->IDPemda).'" class="btn btn-sm btn-info" title="View"><i class="fas fa-eye"></i></a>
                        </div>';
					$data[] = array(
						'unit' 			=> $row->Nm_Unit,
						'luas' 			=> $row->Luas_M2,
						'alamat'		=> $row->Alamat,
						'hak' 			=> $row->Hak_Tanah,
						'penggunaan' 	=> $row->Penggunaan,
						'keterangan' 	=> $row->Keterangan,
						'action' 		=> $action
					);
				}
			}
		}
		
		$data['mydata'] = $data;
		frontendTemplate('frontend/data_tanah', 'Data_tanah', $data, $css, $js);
	}

	public function detail($IDPemda=null){
		$css = [];
		$js = [];
		
		$this->db->select('a.*,b.Nm_Unit, e.Nm_Sub_Unit, f.Nm_UPB')->from('Ta_KIB_A a')
			->join('Ref_Unit b','(a.Kd_Bidang = b.Kd_Bidang And a.Kd_Unit = b.Kd_Unit)')
			->join('Ref_Sub_Unit e','(a.Kd_Bidang = e.Kd_Bidang and 
				a.Kd_Unit = e.Kd_Unit and 
				a.Kd_Sub = e.Kd_Sub)','left')
			->join('Ref_UPB f','(a.Kd_Bidang = f.Kd_Bidang and 
				a.Kd_Unit = f.Kd_Unit and 
				a.Kd_Sub = f.Kd_Sub and 
				a.Kd_UPB = f.Kd_UPB)','left')
			->where('a.IDPemda',$IDPemda);

		if(!isAdmin()){
			$userBidang = session('bidang');
			$userUnit = session('unit');
			$userSubUnit = session('sub');
			$userUpb = session('upb');
			if(!empty($userBidang)) $this->db->where('a.Kd_Bidang',$userBidang);
			if(!empty($userUnit)) $this->db->where('a.Kd_Unit',$userUnit);
			if(!empty($userSubUnit)) $this->db->where('a.Kd_Sub',$userSubUnit);
			if(!empty($userUpb)) $this->db->where('a.Kd_UPB',$userUpb);
		}
		$data['mydata'] =  $this->db->get()->row();
		
		frontendTemplate('frontend/data_tanah_detail', 'Detail Tanah', $data, $css, $js);
	}
}