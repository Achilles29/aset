<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_kendaraan extends MX_Controller {
	
	public function __construct(){
		parent::__construct();
		
	}

	public function index()
	{
		$Nomor_Polisi=get('s');
		$css['css']= ['./assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css'];
		$js['js']= ['./assets/plugins/datatables/jquery.dataTables.js','./assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js'];
		$js['script']= ['frontend/js/js_kendaraan'];		
		$data = array();

		$mydata=[];
		
		if(!empty($Nomor_Polisi)){
			$this->db->select('a.*,b.Nm_Unit, e.Nm_Sub_Unit, f.Nm_UPB, c.Nm_Ruang, c.Jbt_Pngjwb, c.Nip_Pngjwb, c.Nm_Pngjwb, d.Uraian')->from('Ta_KIB_B a')
			->join('Ref_Unit b','(a.Kd_Bidang = b.Kd_Bidang And a.Kd_Unit = b.Kd_Unit)')
			->join('Ta_Ruang c','(a.Kd_Bidang = c.Kd_Bidang and 
				a.Kd_Unit = c.Kd_Unit and 
				a.Kd_Sub = c.Kd_Sub and 
				a.Kd_UPB = c.Kd_UPB and 
				a.Kd_Ruang = c.Kd_Ruang)','left')
			->join('Ref_Sub_Unit e','(a.Kd_Bidang = e.Kd_Bidang and 
				a.Kd_Unit = e.Kd_Unit and 
				a.Kd_Sub = e.Kd_Sub)','left')
			->join('Ref_UPB f','(a.Kd_Bidang = f.Kd_Bidang and 
				a.Kd_Unit = f.Kd_Unit and 
				a.Kd_Sub = f.Kd_Sub and 
				a.Kd_UPB = f.Kd_UPB)','left')
			->join('Ref_Kondisi d','a.Kondisi=d.Kd_Kondisi')
			->where('a.Nomor_Polisi',$Nomor_Polisi);

			// $mydata = $this->db->get()->row();

			$q = $this->db->get();
            if ( $q->num_rows() > 0 ) {
				foreach($q->result() as $row){
					$action = '<div class="box-action">
							<a href="'.site_url('data_kendaraan/detail/'.$row->IDPemda).'" class="btn btn-sm btn-info" title="View"><i class="fas fa-eye"></i></a>
                        </div>';
					$data[] = array(
						'nopol'			=> $row->Nomor_Polisi,
						'perangkat'		=> $row->Nm_Unit,
						'kondisi'		=> $row->Uraian,
						'merek'			=> $row->Merk,
						'type'			=> $row->Type,
						'action' 		=> $action
					);
				}
			}
		}
		
		$data['mydata'] =  $data;
		frontendTemplate('frontend/data_kendaraan', 'Data_kendaraan', $data, $css, $js);
	}

	public function detail($IDPemda=null){
		$css = [];
		$js = [];
		
		$this->db->select('a.*,b.Nm_Unit, e.Nm_Sub_Unit, f.Nm_UPB, c.Nm_Ruang, c.Jbt_Pngjwb, c.Nip_Pngjwb, c.Nm_Pngjwb, d.Uraian')->from('Ta_KIB_B a')
			->join('Ref_Unit b','(a.Kd_Bidang = b.Kd_Bidang And a.Kd_Unit = b.Kd_Unit)')
			->join('Ta_Ruang c','(a.Kd_Bidang = c.Kd_Bidang and 
				a.Kd_Unit = c.Kd_Unit and 
				a.Kd_Sub = c.Kd_Sub and 
				a.Kd_UPB = c.Kd_UPB and 
				a.Kd_Ruang = c.Kd_Ruang)','left')
			->join('Ref_Sub_Unit e','(a.Kd_Bidang = e.Kd_Bidang and 
				a.Kd_Unit = e.Kd_Unit and 
				a.Kd_Sub = e.Kd_Sub)','left')
			->join('Ref_UPB f','(a.Kd_Bidang = f.Kd_Bidang and 
				a.Kd_Unit = f.Kd_Unit and 
				a.Kd_Sub = f.Kd_Sub and 
				a.Kd_UPB = f.Kd_UPB)','left')
			->join('Ref_Kondisi d','a.Kondisi=d.Kd_Kondisi')
			->where('a.IDPemda',$IDPemda);
		if(!isAdmin()){
			$userBidang = session('bidang');
			$userUnit = session('unit');
			$userSubUnit = session('sub');
			$userUpb = session('upb');
			if(!empty($userBidang)) $this->db->where('a.Kd_Bidang',$userBidang);
			if(!empty($userUnit)) $this->db->where('a.Kd_Unit',$userUnit);
			if(!empty($userSubUnit)) $this->db->where('a.Kd_Sub',$userSubUnit);
			if(!empty($userUpb)) $this->db->where('a.Kd_UPB',$userUpb);
		}
		$data['mydata'] =  $this->db->get()->row();
		frontendTemplate('frontend/data_kendaraan_detail', 'Detail Kendaraan', $data, $css, $js);
	}
}
