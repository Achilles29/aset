<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends MX_Controller {
	
	public function __construct(){
		parent::__construct();
		
	}

	public function index()
	{
		$Nomor_Polisi=post('search');
		$css['css']= [];
		$js['js']= [];
		$js['script']= [];
		$data[] = [];
		$mydata=[];
		if(!empty($Nomor_Polisi)){
			$this->db->select('a.*, d.Uraian')->from('Ta_KIB_B a')
				->join('Ref_Unit b','a.Kd_Bidang = b.Kd_Bidang And a.Kd_Unit = b.Kd_Unit')
				->join('Ta_Ruang c','a.Kd_Bidang = c.Kd_Bidang and 
					a.Kd_Unit = c.Kd_Unit and 
					a.Kd_Sub = c.Kd_Sub and 
					a.Kd_UPB = c.Kd_UPB and 
					a.Kd_Ruang = c.Kd_Ruang','left')
				->join('Ref_Kondisi d','a.Kondisi=d.Kd_Kondisi')
				
				->where('a.Nomor_Polisi',$Nomor_Polisi);
			$mydata = $this->db->get()->row();
		}
		
		$data['mydata'] =  $mydata;
		frontendTemplate('frontend/data_kendaraan', 'Search', $data, $css, $js);
	}
	
}
