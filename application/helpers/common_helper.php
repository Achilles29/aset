<?php

function post($val, $filter=true){
	$CI =& get_instance(); 
	return $filter ? $CI->input->post($val, true) : $CI->input->post($val);
}

function get($val, $filter=true){
	$CI =& get_instance(); 
	return $filter ? $CI->input->get($val, true) : $CI->input->get($val);
}

function session($val){
	$CI =& get_instance(); 
	return $CI->session->userdata($val);
}


function ms(array $arr){
	echo json_encode($arr);
	exit;
}

function alias_name($name=null){
	if(is_null($name)) $name = session('name');
	if(empty($name)) return '';
	$char = 2;
	$exp = explode(' ', $name);
	if(count($exp)>1){
		return strtoupper(substr($exp[0], 0,1)) . strtoupper(substr($exp[1], 0,1));
	}elseif(strlen($name)==1){
		return strtoupper($name);
	}else{
		return strtoupper(substr($name, 0,1)) . strtolower(substr($name, 1,1));
	}
}

function option($name){
	$CI =& get_instance();
	$q = $CI->db->get_where('master', ['name'=>$name]);
	return $q->num_rows()==0 ?null: $q->row()->value;
}

function set_option($name, $val){
	$CI =& get_instance();
	$q = $CI->db->get_where('master', ['name'=>$name]);
	if(is_array($val)) $val=json_encode($val);
	if($q->num_rows()==0){
		$CI->db->insert('master', ['value'=>$val, 'name'=>$name]);
	}else{
		$CI->db->update('master', ['value'=>$val], ['name'=>$name]);
	}
}

function tgl_indo($tanggal, $cetak_hari=false, $cetak_jam=false){
	$e = explode(' ',$tanggal);
	$tanggal = $e[0];
	return tanggal_indo($tanggal, $cetak_hari , $cetak_jam);
}

function tanggal_indo($tanggal, $cetak_hari = false, $cetak_jam=false)
{
	$hari = [ 1 =>    'Senin',
				'Selasa',
				'Rabu',
				'Kamis',
				'Jumat',
				'Sabtu',
				'Minggu'
			];
			
	$bulan = [1 =>   'Januari',
				'Februari',
				'Maret',
				'April',
				'Mei',
				'Juni',
				'Juli',
				'Agustus',
				'September',
				'Oktober',
				'November',
				'Desember'
			];
	$ts = strtotime($tanggal);
	$tgl_indo = date('d', $ts) . ' ' . $bulan[ (int)date('m', $ts) ] . ' ' . date('Y', $ts);
	
	if ($cetak_hari) {
		$num = date('N', $ts);
		$tgl_indo= $hari[$num] . ', ' . $tgl_indo;
	}
	if ($cetak_jam) {
		$num = date('H:i', $ts);
		$tgl_indo= $tgl_indo.' '.$num;
	}
	return $tgl_indo;
}

function now($hour=true){
	return $hour?date('Y-m-d H:i:s'):date('Y-m-d');
}

function get_help_option(){
	$CI =& get_instance();
	$CI->load->model('setting/setting_model');
	return $CI->setting_model->get_help_option(1);
}

function get_user_balance(){
	$CI =& get_instance();
	$CI->load->model('user/user_model');
	return number_format($CI->user_model->get_user_balance(),0,',','.');
}


function frontendTemplate($view, $title=null, array $data=[], array $css=[], array $js=[]){
	$CI =& get_instance();
	$data['title'] = $title;
	$data['css'] = $css;
	$data['js'] = $js;
	$CI->template->load('template_frontend', $view, $data);
}

function backendTemplate($view, $title=null, array $data=[], array $css=[], array $js=[]){
	$CI =& get_instance();
	$data['title'] = $title;
	$data['css'] = $css;
	$data['js'] = $js;
	//$CI->template->load('template_backend', $view, $data);
	$CI->template->load('template_newbackend', $view, $data);
}

function authTemplate($view, $title=null, array $data=[], array $css=[], array $js=[]){
	$CI =& get_instance();
	$data['title'] = $title;
	$data['css'] = $css;
	$data['js'] = $js;
	$CI->template->load('template_auth', $view, $data);
}


function permission_view($opt, $val=null){
	if(empty($opt)) return false;
	$arr = json_decode($opt);
	$v = !empty($val)?$val:session('roleId');
	return in_array($v, $arr)?true:false;
}


function isLoggedIn(){
	return session('loggedIn') ? true: false;
}

function isSuperAdmin(){
	return (session('loggedIn') && session('v_group_id')=='Admin') ? true: false;
}

function isAdmin(){
	return (session('loggedIn') && (in_array(session('v_group_id'),['Admin','Supervisor']))) ? true: false;
}

function isUser(){
	return (session('loggedIn') && !isAdmin()) ? true: false;
}


function rupiah($v){
	return number_format($v, 0, ',','.');
}


function curl($url, $referer=null, $ajax=false, $method='GET', array $request=[], array $header = [], $cookie=null, $theproxy=null){
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
	if(!empty($referer)) curl_setopt($curl, CURLOPT_REFERER, $referer);
	curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36");
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
	if($ajax) $header[] = "X-Requested-With: XMLHttpRequest";
	if($method=='POST'){
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($request));
	}
	if(!empty($theproxy)){
		$parse = parse_url($theproxy);
		$proxyaddress = $parse['host'].':'.$parse['port'];
		curl_setopt($curl, CURLOPT_PROXY, $proxyaddress);
		if(isset($parse['user'])){
			$proxyauth = $parse['user'].':'.$parse['pass'];
			curl_setopt($curl, CURLOPT_PROXYUSERPWD, $proxyauth);
		}
	}
	if(!empty($cookie)){
		//curl_setopt( $curl, CURLOPT_COOKIESESSION, true );
		curl_setopt( $curl, CURLOPT_COOKIEJAR, $cookie );
		curl_setopt( $curl, CURLOPT_COOKIEFILE, $cookie );
	}
	
	if(!empty($header)){
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
	}
	$r = curl_exec($curl);
	curl_close($curl);
	return $r;
}

function frontend_assets($path){
	return base_url('assets/frontend/'.$path);
}